# Vemid SMS/Viber API Client

## dependencies:
To run library you need to install a couple of dependencies:

    1. php: ^7.2
    2. giggsey/libphonenumber-for-php: ^8.12
    3. php-http/guzzle6-adapter: ^2.0
    4. psr/http-message: ^1
    5. zendframework/zend-diactoros: ^2.2
    
    dev:
    1. phpunit/phpunit: ^7.2.6
    2. doctrine/coding-standard: ^4
    3. phpstan/phpstan: ^0.10.1

## Instalation

```
composer require vemid/sms-lib
```

## Documentation

    www.vemid.net/sms-lib/documentation_v1.pdf
    
## Basic Examples
Example of sending sms to singe number without scheduleOn

    use Vemid\Sms;
    

    try {
        $client = new Sms\Client('username', 'pasword');
        $messages = $client->messages->send('SendeName','phone', 'text');
        print_r($messages);
        $test = 0;
    } catch (\Exception $e) {
        print_r($e->getMessage());
    }

Example of sending sms to multiple number without scheduleOn

    use Vemid\Sms;
    
    $recipeints = ['phone1','phone2',....];

    try {
        $client = new Sms\Client('username', 'pasword');
        $messages = $client->messages->sendBatch('SendeName','phone', 'text');
        print_r($messages);
        $test = 0;
    } catch (\Exception $e) {
        print_r($e->getMessage());
    }

Sending with scheduleOn , you must set an DateTime object


###These packages are supplied by third parties, and their quality can not be guaranteed.


&copy; VemidSoft development
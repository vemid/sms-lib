<?php

declare(strict_types=1);

namespace Vemid\Sms\Exceptions;

/**
 * Class MethodNotImplementedException
 */
class MethodNotImplementedException extends \RuntimeException
{
    /**
     * @param string $methodName
     * @return MethodNotImplementedException
     */
    public static function fromMethodName(string $methodName): \RuntimeException
    {
        return new self(sprintf('Method with name %s not implements', $methodName));
    }
}

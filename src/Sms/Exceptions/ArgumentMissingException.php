<?php

declare(strict_types=1);

namespace Vemid\Sms\Exceptions;

/**
 * Class ArgumentMissingException
 * @package Vemid\Sms\Exceptions
 */
class ArgumentMissingException extends \RuntimeException
{
    /**
     * @param string $argumentName
     * @return MethodNotImplementedException
     */
    public static function fromArgument(string $argumentName): \RuntimeException
    {
        return new self(sprintf('Argument %s is missing', $argumentName));
    }
}

<?php

declare(strict_types=1);

namespace Vemid\Sms\Exceptions;

/**
 * Class ResourceNotSupportedException
 * @package Vemid\Sms\Exceptions
 */
class ResourceNotSupportedException extends \DomainException
{
    /**
     * @param string $resourceName
     * @return ResourceNotSupportedException
     */
    public static function fromResourceName(string $resourceName)
    {
        return new self(sprintf('Resource %s is not supported', $resourceName));
    }
}

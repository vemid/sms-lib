<?php

declare(strict_types=1);

namespace Vemid\Sms\Exceptions;

/**
 * Class NotAuthorizedException
 * @package Vemid\Sms\Exceptions
 */
class NotAuthorizedException extends \Exception
{
    /**
     * @return \Exception
     */
    public static function fromResponse(): \Exception
    {
        return new self(sprintf('Not authorized!'), 401);
    }
}

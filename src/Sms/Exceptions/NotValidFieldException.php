<?php

declare(strict_types=1);

namespace Vemid\Sms\Exceptions;

/**
 * Class NotValidFieldException
 * @package Vemid\Sms\Exceptions
 */
class NotValidFieldException extends \Exception
{
    /**
     * @param $fieldName
     * @param null $additionalInfo
     * @return \Exception
     */
    public static function fromValidator($fieldName, $additionalInfo = null): \Exception
    {
        return new self(sprintf('%s is not valid!%s', $fieldName, $additionalInfo));
    }
}

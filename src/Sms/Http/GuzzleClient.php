<?php

declare(strict_types=1);

namespace Vemid\Sms\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Psr\Http\Message\ResponseInterface;
use Vemid\Sms\Exceptions\HttpException;
use Vemid\Sms\Resources\ResourceInterface;
use Zend\Diactoros\Request;
use Zend\Diactoros\Response;

/**
 * Class GuzzleClient
 */
final class GuzzleClient implements ClientInterface
{
    /** @var Client */
    private $client;

    private $version = 'v1';

    /** @var string */
    private $baseUrl;

    /** @var string */
    private $jwtToken;

    /**
     * GuzzleClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * {@inheritDoc}
     */
    public function setBaseUrl(string $url): void
    {
        $this->baseUrl = $url;
    }

    /**
     * {@inheritDoc}
     */
    public function setToken(string $token): void
    {
        $this->jwtToken = $token;
    }

    /**
     * {@inheritDoc}
     */
    public function requestToken(string $user, string $password): ResponseInterface
    {
        return $this->client->send(new Request($this->buildUrl(), 'GET'), ['auth' => [$user, $password]]);
    }

    /**
     * {@inheritDoc}
     */
    public function request(ResourceInterface $resource, $method = 'POST', array $data = [], array $params = []): Response
    {
        $url = $this->buildUrl($resource->getResourceUrl());
        $data = ['form_params' => $data];

        if ($params) {
            $options['query'] = $params;
        }
        try {
            $request = new Request($url, $method);
            $request = $request->withHeader('Authorization', sprintf('Bearer %s',$this->jwtToken));
            $response = $this->client->send($request, $data);

        } catch (BadResponseException $exception) {
            $response = $exception->getResponse();
        } catch (\Exception $exception) {
            throw new HttpException('Unable to complete the HTTP request', 500);
        }

        return new Response($response->getBody(), $response->getStatusCode(), $response->getHeaders());
    }

    /**
     * @param string $resourceUrl
     * @return string
     */
    private function buildUrl(string $resourceUrl = ''): string
    {
        return sprintf(
            'http://%s%s/%s',
            rtrim($this->baseUrl, '/'),
            $this->jwtToken ? '/' . $this->version: '',
            str_replace('.', '/', $resourceUrl)
        );
    }
}

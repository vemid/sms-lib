<?php

declare(strict_types=1);

namespace Vemid\Sms\Http;

use Psr\Http\Message\ResponseInterface;
use Vemid\Sms\Exceptions\HttpException;
use Vemid\Sms\Resources\ResourceInterface;
use Zend\Diactoros\Response;

/**
 * Interface ClientInterface
 * @package Vemid\Sms\Http
 */
interface ClientInterface
{
    /**
     * @param string $user
     * @param string $password
     * @return ResponseInterface
     */
    public function requestToken(string $user, string $password): ResponseInterface;

    /**
     * @param string $url
     */
    public function setBaseUrl(string $url): void;

    /**
     * @param string $token
     */
    public function setToken(string $token): void;

    /**
     * @param ResourceInterface $resource
     * @param string $method
     * @param array $params
     * @param array $data
     * @return Response
     * @throws HttpException
     */
    public function request(ResourceInterface $resource, $method = 'GET', array $data = [], array $params = []): Response;
}

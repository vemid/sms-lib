<?php

declare(strict_types=1);

namespace Vemid\Sms\Validators;

use Vemid\Sms\Exceptions\NotValidFieldException;

/**
 * Interface ValidatorInterface
 * @package Vemid\Sms\Validators
 */
interface ValidatorInterface
{
    /**
     * @param $input
     * @return bool
     * @throws NotValidFieldException
     */
    public function validate(&$input): bool;
}
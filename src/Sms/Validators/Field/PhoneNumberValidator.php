<?php

declare(strict_types=1);

namespace Vemid\Sms\Validators\Field;

use Vemid\Sms\Validators\ValidatorInterface;

/**
 * Class PhoneNumberValidator
 * @package Vemid\Sms\Validators
 */
class PhoneNumberValidator implements ValidatorInterface
{
    /**
     * {@inheritDoc}
     */
    public function validate(&$number): bool
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $swissNumberProto = $phoneUtil->parse($number, "RS");

        return $phoneUtil->isValidNumberForRegion($swissNumberProto, 'RS');
    }
}
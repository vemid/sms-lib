<?php

declare(strict_types=1);

namespace Vemid\Sms\Validators;

use Vemid\Sms\Exceptions\NotValidFieldException;
use Vemid\Sms\Validators\Field\PhoneNumberValidator;

/**
 * Class BulkMessages
 * @package Vemid\Sms\Validators
 */
class SingleMessageValidator implements ValidatorInterface
{
    /**
     * {@inheritDoc}
     */
    public function validate(&$payload): bool
    {
        $phoneValidator = new PhoneNumberValidator();

        if (is_string($payload['sender'])) {
            if (empty($payload['sender'])) {
                throw NotValidFieldException::fromValidator('Sender', 'Sender is empty!');
            }
        } else {
            if (!$phoneValidator->validate($payload['sender'])) {
                throw NotValidFieldException::fromValidator('Sender');
            }
        }

        if (empty($payload['recipient'])) {
            throw NotValidFieldException::fromValidator('Recipient', 'No Recipient found!');
        }

        if (!$phoneValidator->validate($payload['recipient'])) {
            throw NotValidFieldException::fromValidator('Recipient');
        }

        if (empty($payload['text'])) {
            throw NotValidFieldException::fromValidator('Text', 'Message body is empty!');
        }

        if (isset($payload['scheduledOn'])) {
            if (!$payload['scheduledOn'] instanceOf \DateTime) {
                if (!$payload['scheduledOn'] = \DateTime::createFromFormat('Y-m-d', $payload['scheduledOn'])) {
                    throw NotValidFieldException::fromValidator('Schedule On', 'Wring date format!');
                }
            }

            $payload['scheduledOn'] = $payload['scheduledOn']->format('Y-m-d H:i:s');
        }

        return true;
    }
}

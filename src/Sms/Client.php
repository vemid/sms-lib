<?php

declare(strict_types=1);

namespace Vemid\Sms;

use Vemid\Sms\Exceptions\MethodNotImplementedException;
use Vemid\Sms\Http\ClientInterface;
use Vemid\Sms\Http\GuzzleClient;
use Vemid\Sms\Resources\Account\AccountProvider;
use Vemid\Sms\Resources\Messages\MessageProvider;

/**
 *
 * @property MessageProvider $messages
 * @property AccountProvider $account
 * @property Api $api
 *
 * Class Client
 */
class Client
{
    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /** @var ClientInterface|null */
    private $httpClient;

    /**
     * Client constructor.
     * @param string $username
     * @param string $password
     * @param ClientInterface|null $client
     */
    public function __construct(string $username, string $password, ClientInterface $client = null)
    {
        $this->username = $username;
        $this->password = $password;
        $this->httpClient = $client;
    }

    /**
     * @return ClientInterface
     */
    public function getHttpClient(): ClientInterface
    {
        return $this->httpClient;
    }

    /**
     * @param ClientInterface $httpClient
     */
    public function setHttpClient(ClientInterface $httpClient): void
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @return Api
     */
    private function getApi()
    {
        return new Api(($this->httpClient ?: new GuzzleClient()), $this->username, $this->password);
    }

    /**
     * @return MessageProvider
     */
    private function getMessages()
    {
        return $this->api->getMessages();
    }

    /**
     * @return AccountProvider
     */
    private function getAccount()
    {
        return $this->api->getAccount();
    }


    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        $method = 'get' . \ucfirst($name);
        if (\method_exists($this, $method)) {
            return $this->$method();
        }

        throw MethodNotImplementedException::fromMethodName($method);
    }
}

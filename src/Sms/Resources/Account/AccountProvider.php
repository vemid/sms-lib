<?php

declare(strict_types=1);

namespace Vemid\Sms\Resources\Account;

use Vemid\Sms\Resources\AbstractResource;
use Vemid\Sms\Resources\Messages\Balance;

/**
 * Class Provider
 * @package Vemid\Sms\Resources\Messages
 */
final class AccountProvider extends AbstractResource
{
    public function getBalance()
    {
        return new Balance();
    }

    /**
     * @inheritDoc
     */
    protected function getResourceUrl(): string
    {
        return 'account';
    }
}
<?php

declare(strict_types=1);

namespace Vemid\Sms\Resources\Messages;

use Vemid\Sms\Validators\BulkMessagesValidator;

/**
 * Class Multiple
 * @package Vemid\Sms\Resources\Messages
 */
class Multiple extends MessageProvider
{
    /**
     * @return string
     */
    public function getResourceUrl(): string
    {
        return parent::getResourceUrl() . '.multi';
    }

    /**
     * @param string $sender
     * @param array $recipients
     * @param string $text
     * @param \DateTime|null $scheduledOn
     * @return Multiple|void
     * @throws \Vemid\Sms\Exceptions\HttpException
     * @throws \Vemid\Sms\Exceptions\NotValidFieldException
     */
    public function sendBatch(string $sender, array $recipients, string $text, \DateTime $scheduledOn = null)
    {
        $payload = compact(['sender', 'recipients', 'text', 'scheduledOn']);

        $validator = new BulkMessagesValidator();
        if ($validator->validate($payload)) {
            $this->client->request($this, 'POST', $payload);
        }
    }
}

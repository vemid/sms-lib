<?php

declare(strict_types=1);

namespace Vemid\Sms\Resources\Messages;

use Vemid\Sms\Validators\BulkMessagesValidator;
use Vemid\Sms\Validators\SingleMessageValidator;

/**
 * Class Single
 * @package Vemid\Sms\Resources\Messages
 */
final class Single extends MessageProvider
{

    /**
     * @param string $sender
     * @param string $recipient
     * @param string $text
     * @param \DateTime|null $scheduledOn
     * @return mixed|void
     * @throws \Vemid\Sms\Exceptions\HttpException
     * @throws \Vemid\Sms\Exceptions\NotValidFieldException
     */
    public function send(string $sender, string $recipient, string $text, \DateTime $scheduledOn = null)
    {
        $payload = compact(['sender', 'recipient', 'text', 'scheduledOn']);

        $validator = new SingleMessageValidator();
        if ($validator->validate($payload)) {
            $this->client->request($this, 'POST', $payload);
        }
    }

    /**
     * @return string
     */
    public function getResourceUrl(): string
    {
        return parent::getResourceUrl() . '.single';
    }
}

<?php

declare(strict_types=1);

namespace Vemid\Sms\Resources\Messages;

use Vemid\Sms\Resources\AbstractResource;

/**
 *
 * Class Message
 * @package Vemid\Sms\Resources\Messages
 */
class MessageProvider extends AbstractResource
{
    /**
     * @param string $sender
     * @param array $recipients
     * @param string $text
     * @param \DateTime|null $scheduledOn
     * @return Multiple|void
     * @throws \Vemid\Sms\Exceptions\HttpException
     * @throws \Vemid\Sms\Exceptions\NotValidFieldException
     */
    public function sendBatch(string $sender, array $recipients, string $text, \DateTime $scheduledOn = null) {
        return (new Multiple($this->client))->sendBatch($sender, $recipients, $text, $scheduledOn);
    }

    /**
     * @param string $sender
     * @param string $recipient
     * @param string $text
     * @param \DateTime|null $scheduledOn
     * @return mixed|void
     * @throws \Vemid\Sms\Exceptions\HttpException
     * @throws \Vemid\Sms\Exceptions\NotValidFieldException
     */
    public function send(string $sender, string $recipient, string $text, \DateTime $scheduledOn = null) {
        return (new Single($this->client))->send($sender, $recipient, $text, $scheduledOn);
    }

    /**
     * @return string
     */
    protected function getResourceUrl(): string
    {
        return 'messages';
    }
}

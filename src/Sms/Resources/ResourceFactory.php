<?php

declare(strict_types=1);

namespace Vemid\Sms\Resources;

use Vemid\Sms\Exceptions\ResourceNotSupportedException;
use Vemid\Sms\Http\ClientInterface;
use Vemid\Sms\Resources\Account\AccountProvider;
use Vemid\Sms\Resources\Messages\MessageProvider;

/**
 * Class ResourceFactory
 * @package Vemid\Sms\Resources
 */
class ResourceFactory
{
    /** @var ClientInterface */
    private $client;

    /**
     * ResourceFactory constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $resourceName
     * @param null $arguments
     * @return AbstractResource
     */
    public function getResource(string $resourceName, $arguments = null): ResourceInterface
    {
        $resources = [
            'account' => AccountProvider::class,
            'messages' => MessageProvider::class
        ];

        if (isset($resources[$resourceName])) {
            return new $resources[$resourceName]($this->client, $arguments);
        }

        throw ResourceNotSupportedException::fromResourceName($resourceName);
    }
}

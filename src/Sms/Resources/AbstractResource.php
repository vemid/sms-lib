<?php

declare(strict_types=1);

namespace Vemid\Sms\Resources;

use Vemid\Sms\Http\ClientInterface;

/**
 * Class AbstractResource
 * @package Vemid\Sms\Resources
 */
abstract class AbstractResource implements ResourceInterface
{
    /** @var ClientInterface   */
    protected $client;

    /**
     * AbstractResource constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    abstract protected function getResourceUrl(): string;
}

<?php

declare(strict_types=1);

namespace Vemid\Sms;

use GuzzleHttp\Exception\RequestException;
use Vemid\Sms\Exceptions\MethodNotImplementedException;
use Vemid\Sms\Exceptions\NotAuthorizedException;
use Vemid\Sms\Http\ClientInterface;
use Vemid\Sms\Resources\Account\AccountProvider;
use Vemid\Sms\Resources\Messages\MessageProvider;
use Vemid\Sms\Resources\ResourceFactory;

/**
 * @method AccountProvider getAccount
 * @method  MessageProvider getMessages
 *
 *
 * Class Api
 * @package Vemid\Sms
 */
class Api
{
    /** @var string */
    private $domain = 'localhost:90';

    /** @var string */
    private $version = 'v1';

    /** @var ClientInterface  */
    private $client;

    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /**
     * Api constructor.
     * @param ClientInterface $client
     * @param string $username
     * @param string $password
     */
    public function __construct(ClientInterface $client, string $username, string $password)
    {
        $this->client = $client;
        $this->client->setBaseUrl($this->domain);

        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name) {
        $method = 'get' . \ucfirst($name);
        if (\method_exists($this, $method)) {
            return $this->$method();
        }

        throw MethodNotImplementedException::fromMethodName($method);
    }

    /**
     * @param $method
     * @param null $arguments
     * @return Resources\AbstractResource
     * @throws \Exception
     */
    public function __call($method, $arguments = null)
    {
        if (strpos($method, 'get') !== false) {
            $method = strtolower(str_replace('get', '', $method));
        }

        try {
            $response = $this->client->requestToken($this->username, $this->password);
            $jwtToken = $response->getHeader('Auth-Token');
            $this->client->setToken($jwtToken[0]);
        } catch (RequestException $exception) {
            $code = $exception->getResponse()->getStatusCode();
            $this->resolveResponseExceptions($code, (string)$exception->getResponse()->getBody());
        }

        $resourceFactory = new ResourceFactory($this->client);

        return $resourceFactory->getResource($method, $arguments);
    }

    /**
     * @param $code
     * @param $message
     * @throws \Exception
     */
    private function resolveResponseExceptions($code, $message)
    {
        switch ($code) {
            case 401:
            default:
                throw NotAuthorizedException::fromResponse();
        }
    }
}
